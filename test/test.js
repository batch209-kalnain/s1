const { assert } = require('chai');
const { newUser } = require('../index.js');

// describe - give structure to your test suite.
describe('Test newUser Object',() => {

	// unit test - the actual test.
	// it('Assert newUser type is object',()=> {
	// 	assert.equal(typeof(newUser),'object')
	// });

	// it('Assert newUser.email is type string',()=> {
	// 	assert.equal(typeof(newUser.email),'string')
	// });

	// // .notEqual - testing for inequality.
	// it('Assert newUser.email is not undefined',()=>{
	// 	assert.notEqual(typeof(newUser.email),undefined)
	// });

	// // .isAtLeast - testing for greater than or equal to >=
	// it('Assert that the number of characters in newUser.password is at least 16',()=>{
	// 	assert.isAtLeast(newUser.password.length,16)
	// });


	  ///////////////////////
	 // Activity Solution //
	///////////////////////

	// #1
	it('Assert newUser.firstName type is string',()=> {
		assert.equal(typeof(newUser.firstName),'string')
	});

	// #2
	it('Assert newUser.lastName type is string',()=> {
		assert.equal(typeof(newUser.lastName),'string')
	});

	// #3 
	it('Assert newUser.firstName is not undefined',()=> {
		assert.notEqual(typeof(newUser.firstName),'undefined')
	});

	// #4
	it('Assert newUser.lastName is not undefined',()=> {
		assert.notEqual(typeof(newUser.lastName),'undefined')
	});

	// #5
	it('Assert that the newUser.age is at least 18',()=> {
		assert.isAtLeast(newUser.age,18)
	});

	// #6
	it('Assert newUser.age type is number',()=> {
		assert.equal(typeof(newUser.age),'number')
	});

	// #7
	it('Assert newUser.contactNumber type is string',()=> {
		assert.equal(typeof(newUser.contactNumber),'string')
	});

	// #8
	it('Assert that the number of characters in newUser.contactNumber is at least 11',()=> {
		assert.isAtLeast(newUser.contactNumber.length,11)
	});

	// #9
	it('Assert newUser.batchNumber type is number',()=> {
		assert.equal(typeof(newUser.batchNumber),'number')
	});

	// #10
	it('Assert newUser.batchNumber is not undefined',()=> {
		assert.notEqual(typeof(newUser.batchNumber),'undefined')
	});
});